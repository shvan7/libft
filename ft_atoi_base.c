/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 14:31:50 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/23 14:59:36 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_check_base(char *base)
{
	if (ft_strcspn(base, "-+") > -1)
		return (-1);
	while (*base++)
		if (ft_strchr(base, *(base - 1)))
			return(-1);
	return (0);
}

int		ft_check_str(char *str, char *base)
{
	int count;

	count = -1;
	if (ft_issign(*str))
		str++;
	while (*str)
		if (!(ft_strchr(base, (int)*str++)))
			return (count);
		else
			count++;
	return (count);
}

int		ft_converter(char *str, char *base, int indice)
{
	int sum;
	int i;
	
	if ((i = ft_strcspn(base, str)) < 0)
		return (0);
	sum =  i * ft_power(ft_strlen(base), indice);
	return (ft_converter(str + 1, base, indice - 1) + sum);
}

int		ft_atoi_base(char *str, char *base)
{
	int result;
	int len;

	result = 0;
	if (ft_check_base(base) < 0)
		return (0);
	if ((len = ft_check_str(str, base)) < 0)
		return (0);
	if (str[0] == '-')
		result = -1 * ft_converter(str + 1, base, len);
	else if (str[0] == '+')
		result = ft_converter(str + 1, base, len);
	else
		result = ft_converter(str, base, len);
	return (result);
}
