/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 19:54:24 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:28:49 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dest, const char *src)
{
	char *cp;

	cp = dest;
	while (*dest)
		dest++;
	while (*src)
		*dest++ = *src++;
	*dest++ = '\0';
	return (cp);
}
