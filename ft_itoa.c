/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 10:00:34 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 15:27:36 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void			ft_build_str(int n, char *p)
{
	if (n == 0)
		return ;
	*p = -(n % 10) + 48;
	return (ft_build_str(n / 10, p - 1));
}

char				*ft_itoa(int n)
{
	unsigned int		len;
	char				*p;

	len = n < 0 ? ft_countdec(n) + 1 : ft_countdec(n);
	if (!(p = ft_strnew(len)))
		return (NULL);
	if (n < 0)
		p[0] = '-';
	if (n == 0)
	{
		p[0] = '0';
		return (p);
	}
	ft_build_str(n > 0 ? -n : n, p + len - 1);
	return (p);
}
