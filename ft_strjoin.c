/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 06:27:11 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:42:26 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	unsigned int	len_p;
	char			*p;
	char			*save;

	if (!s1 || !s2)
		return (NULL);
	len_p = ft_strlen(s1) + ft_strlen(s2);
	if (!(p = (char *)malloc((len_p + 1) * sizeof(char))))
		return (NULL);
	save = p;
	while (len_p--)
		*p++ = *s1 ? *s1++ : *s2++;
	*p = '\0';
	return (save);
}
