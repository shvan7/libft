/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tokenlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 11:31:01 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 15:36:30 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_tokenlen(char const *s, char c)
{
	unsigned int count;

	count = 0;
	if (!s)
		return (1);
	s = ft_strskip(s, c);
	while (*s)
	{
		while (*s != c && *s)
			s++;
		s = ft_strskip(s, c);
		count++;
	}
	return (count);
}
