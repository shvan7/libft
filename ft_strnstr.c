/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 04:59:21 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:31:43 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strnstr(const char *str, const char *find, size_t n)
{
	size_t i;
	size_t j;

	i = 0;
	j = 0;
	if (find[0] == '\0')
		return (char *)(str);
	while (str[i] != find[j] && str[i])
		i++;
	while (str[i] && i < n)
	{
		while (str[i] == find[j] && str[i] && i < n)
		{
			if (find[j + 1] == '\0')
				return ((char *)(str + i - j));
			i++;
			j++;
		}
		i = i - j + 1;
		j = 0;
	}
	return (NULL);
}
