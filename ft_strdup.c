/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 11:54:04 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:41:14 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strdup(const char *s)
{
	char			*cpy;
	unsigned int	len;

	len = ft_strlen(s) + 1;
	if (!(cpy = (char *)malloc(len * sizeof(char))))
		return (NULL);
	ft_strcpy(cpy, s);
	return (cpy);
}
