/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 10:39:08 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/23 14:55:28 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_converter(int nbr, char *base, int base_n)
{
	int i;

	i = nbr < 0 ? -(nbr % base_n) : nbr % base_n;
	if (nbr == 0)
		return (1);
	ft_converter(nbr / base_n, base, base_n);
	ft_putchar(base[i]);
	return (0);
}

static int		ft_check_base(char *base)
{
	int i;
	int j;

	i = -1;
	j = 0;
	while (base[++i])
	{
		while (base[++j])
		{
			if (base[i] == base[j])
				return (0);
			if (base[i] == '-' || base[i] == '+')
				return (0);
		}
		j = i + 1;
	}
	return (1);
}

void	ft_putnbr_base(int nbr, char *base)
{
	int base_n;

	base_n = ft_strlen(base);
	if (base_n > 1 && ft_check_base(base))
	{
		if (nbr < 0)
			ft_putchar('-');
		if (nbr == 0)
			ft_putchar(base[0]);
		else
			ft_converter(nbr, base, base_n);
	}
}
