/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 14:35:20 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:26:00 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	char			*dd;
	const char		*ss;
	unsigned int	i;

	i = 0;
	ss = src;
	dd = dest;
	if (ss < dd)
		while (n-- > 0)
			dd[n] = ss[n];
	if (ss > dd)
		while (i < n)
		{
			dd[i] = ss[i];
			i++;
		}
	return (dd);
}
