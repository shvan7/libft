/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 18:51:37 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:55:47 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void		*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned char		*dd;
	const unsigned char	*ss;

	ss = src;
	dd = dest;
	while (n--)
		dd[n] = ss[n];
	return ((void *)dd);
}
