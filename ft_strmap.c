/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 07:52:49 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:39:42 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char			*p;
	unsigned int	i;

	i = 0;
	if (!s || !(p = ft_strdup(s)))
		return (NULL);
	while (p[i])
	{
		p[i] = (*f)(p[i]);
		i++;
	}
	return (p);
}
