/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcspn.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 08:06:23 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/21 14:23:39 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcspn(char const *s, char const *charset)
{
	unsigned int i;

	i = 0;
	if (s && charset)
		while (*charset)
		{
			i = 0;
			while (s[i])
				if (s[i++] == *charset)
					return (--i);
			charset++;
		}
	return (-1);
}