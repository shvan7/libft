/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 13:49:06 by aulima-f          #+#    #+#             */
/*   Updated: 2018/08/16 15:15:10 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#define SIGNE str[i] == '-' || str[i] == '+'
#define SIGNE1 (str[i] != '-' && str[i] != '+')

int		ft_find_index(char c, char *base)
{
	int i;

	i = -1;
	while (base[++i])
		if (base[i] == c)
			return (i);
	return (-1);
}

int		ft_chkb_len(char *str, int len)
{
	int i;
	int j;

	i = -1;
	j = 0;
	if (len)
	{
		while (str[j] && str[j] != '-' && str[j] != '+')
			j++;
		return (j);
	}
	while (str[++i])
	{
		while (str[++j])
			if (str[i] == str[j] || SIGNE)
				return (0);
		j = i + 1;
	}
	return (i);
}

int		ft_check_data(char *str, char *base_from, char *base_to)
{
	int i;

	i = -1;
	while (str[++i])
		if (ft_find_index(str[i], base_from) < 0 && SIGNE1)
			return (0);
	if (!ft_chkb_len(base_to, 0) || !ft_chkb_len(base_from, 0))
		return (0);
	if (ft_chkb_len(base_to, 1) < 2 || ft_chkb_len(base_from, 1) < 2)
		return (0);
	return (1);
}

int		ft_atoi_base(char *str, char *base)
{
	int base_n;
	int result;
	int i;

	base_n = ft_chkb_len(base, 1);
	result = 0;
	i = -1;
	if (str[0] == '-' || str[0] == '+')
		i++;
	while (str[++i] && SIGNE1)
		result = result * base_n + ft_find_index(str[i], base);
	return (str[0] == '-' ? -result : result);
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		i;
	int		size;
	int		base_n;
	int		sum;
	char	*ptr;

	if (!ft_check_data(nbr, base_from, base_to))
		return (ptr = 0);
	sum = ft_atoi_base(nbr, base_from);
	size = nbr < 0 ? 1 : 0;
	base_n = ft_chkb_len(base_to, 1);
	i = sum;
	while (i /= base_n)
		size++;
	size = sum < 0 ? size + 2 : size + 1;
	ptr = malloc(sizeof(char) * size + 1);
	*ptr = sum < 0 ? '-' : '0';
	ptr[size] = 0;
	while (sum)
	{
		ptr[--size] = base_to[sum < 0 ? -(sum % base_n) : sum % base_n];
		sum /= base_n;
	}
	return (ptr);
}
